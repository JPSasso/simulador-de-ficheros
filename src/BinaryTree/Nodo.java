package BinaryTree;

public class Nodo {	
	
	private Nodo izq ;
	private Nodo der ;
	private int Indice;	
	private FileHandler Archivo;
	
	public Nodo(int dato2) {
		this.Indice = dato2;
	}

	public Nodo(int indice, FileHandler f) {
		this.Indice = indice;
		this.Archivo = f;
	}

	public int getDato () { return Indice;}
	public void setDato(int dato) {	this.Indice = dato;}
	public Nodo getDer() {return der;}
	public void setDer(Nodo der) { this.der = der;}
	public Nodo getIzq() { return izq;}
	public void setIzq(Nodo izq) { this.izq = izq;}

	public FileHandler getArchivo() {
		return Archivo;
	}

	public void setArchivo(FileHandler archivo) {
		Archivo = archivo;
	}
}