package BinaryTree;
import java.awt.Desktop;
import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		BinaryTree Arbol = new BinaryTree();	
		
		Arbol.Insertar(new Nodo(1,new FileHandler()));
		if (Arbol.GetNodo(1).getArchivo().CreateFile(""
				+ "C:\\Users\\user\\Documents\\archivos importantes\\programacion\\"
				+ "Lenguajes\\Java\\Arbol binario de busqueda de archivos\\Test 1.txt") == 1)
		{
			Arbol.GetNodo(1).getArchivo().WriteFile("Juan Pablo Sasso");
		}
		Arbol.Insertar(new Nodo(3,new FileHandler()));
		if (Arbol.GetNodo(3).getArchivo().CreateFile(""
				+ "C:\\Users\\user\\Documents\\archivos importantes\\programacion\\"
				+ "Lenguajes\\Java\\Arbol binario de busqueda de archivos\\Test 3.txt") == 1)
		{
			Arbol.GetNodo(3).getArchivo().WriteFile("Marianella Cabrera");
		}
		Arbol.Insertar(new Nodo(2,new FileHandler()));
		if (Arbol.GetNodo(2).getArchivo().CreateFile(""
				+ "C:\\Users\\user\\Documents\\archivos importantes\\programacion\\"
				+ "Lenguajes\\Java\\Arbol binario de busqueda de archivos\\Test 2.txt") == 1)
		{
			Arbol.GetNodo(2).getArchivo().WriteFile("Federico Roldan");
		}
		System.out.println("\nInOrder");
		Arbol.InOrder();
		
		Scanner scaner = new Scanner(System.in);		
		System.out.println("\n\nInsert the File ID: ");
		int Userid = scaner.nextInt();
		Nodo UserNode = Arbol.GetNodo(Userid);		
		try {
			if (! Desktop.isDesktopSupported()) { 
				System.out.println("Desktop Not suported"); 
				return;
			}
			Desktop dskp = Desktop.getDesktop();
			if (UserNode.getArchivo().getFile().exists())
			{
				dskp.open(UserNode.getArchivo().getFile());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		scaner.close();
	}
}
