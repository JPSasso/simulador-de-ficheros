package BinaryTree;

public class BinaryTree {

	Nodo Raiz;
	
	public void Insertar(Nodo dato)
	{
		if (this.Raiz == null)
		{
			this.Raiz = dato; 
		}
		else
		{
			this.Insertar(this.Raiz,dato);
		}
	}
	private void Insertar(Nodo padre, Nodo dato) {
		if (dato.getDato() > padre.getDato())
		{
			if(padre.getDer() != null) {
				this.Insertar(padre.getDer(), dato);
			}
			else
			{
				padre.setDer(dato);
			}			
		}
		else {
			if(padre.getIzq() != null)
			{
				this.Insertar(padre.getIzq(), dato);
			}
			else
			{
				padre.setIzq(dato);
			}
		}
	}
	private void Mostrar(Nodo n)
	{
		System.out.print(n.getDato()+ "|");
	}
	private void Preorder(Nodo n)
	{
		if (n!=null)
		{
			Mostrar(n);
			Preorder(n.getIzq());
			Preorder(n.getDer());
		}
	}
	private void Inorder(Nodo n)
	{
		if(n!=null)
		{
			Inorder(n.getIzq());
			Mostrar(n);
			Inorder(n.getDer());
		}
	}
	private void Postorder(Nodo n)
	{
		if (n!= null)
		{
			Postorder(n.getIzq());
			Postorder(n.getDer());
			Mostrar(n);
		}
	}
	public void InOrder()
	{this.Inorder(this.Raiz);}
	public void PreOrder()
	{
		this.Preorder(this.Raiz);
	}
	public void PostOrder()
	{
		this.Postorder(this.Raiz);
	}
	
	public Nodo GetNodo(int Id)
	{
		return GetNodo(this.Raiz, Id);
	}
	private Nodo GetNodo(Nodo n, int id) {
		if (n == null)
		{
			return null;
		}
		if (n.getDato() == id)
		{
			return n;
		}
		else if (id < n.getDato())
		{
			return GetNodo(n.getIzq(),id);
		}
		else
		{
			return GetNodo(n.getDer(),id);
		}
	}
	public boolean Exists (int busqueda)
	{
		return Exists(this.Raiz, busqueda);
	}	
	
	private boolean Exists(Nodo n, int busqueda) {
		if (n == null)
		{
			return false;
		}
		if (n.getDato() == busqueda)
		{
			return true;
		}
		else if (busqueda < n.getDato())
		{
			return Exists(n.getIzq(),busqueda);
		}
		else
		{
			return Exists(n.getDer(),busqueda);
		}
	}
}

