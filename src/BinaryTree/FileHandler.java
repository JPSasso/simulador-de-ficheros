package BinaryTree;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileHandler {
	
	private File file;
	public File getFile() {return this.file;}
	
	public int CreateFile(String name)
	{
		try
		{
			this.file = new File(name);
			if (file.createNewFile())
			{
				// archivo creado
				System.out.println("File created: " + this.file.getName());
				return 1;
			}
			else
			{
				// archivo ya existente
				System.out.println("File " +this.file.getName()+ " already exists.");
				return 0;
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return -1;
		}
	}
	public boolean WriteFile(String text)
	{
		try {
			FileWriter escritor = new FileWriter(file);
			escritor.write(text);			
			escritor.close();
			return true;
		} 
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	public String ReadFile (File f)
	{
		try 
		{
			Scanner scaner = new Scanner(f);
			String ret = scaner.nextLine();
			scaner.close();
			return ret;
		} 		
		catch (FileNotFoundException e) {		
			e.printStackTrace();
			return "Error de lectura de archivo";
		}		
		
	}
}